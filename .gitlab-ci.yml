---
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG

image: python:3.10.14

stages:
  - lint
  - build
  - test
  - deploy-review
  - validate
  - renovate_bot
  - release
  - ingest
  - runway_staging
  - runway_production

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  POETRY_CACHE_DIR: "$CI_PROJECT_DIR/.cache/poetry"

  DOCKER_VERSION: "20.10.23"
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_MODEL_GATEWAY: "$CI_REGISTRY_IMAGE/model-gateway"
  DOCKER_INGEST: "$CI_REGISTRY_IMAGE/ingest"

  TARGET_IMAGE: "$DOCKER_MODEL_GATEWAY:$CI_COMMIT_SHORT_SHA"
  INGEST_IMAGE: "$DOCKER_INGEST:$CI_COMMIT_SHORT_SHA"

  SAST_EXCLUDED_PATHS: "tests, tmp, api"

include:
  - local: .gitlab/ci/build.gitlab-ci.yml
  - local: .gitlab/ci/lint.gitlab-ci.yml
  - local: .gitlab/ci/release.gitlab-ci.yml
  - local: .gitlab/ci/ingest.gitlab-ci.yml
  - template: Jobs/Container-Scanning.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

  # Upgrades dependencies on a schedule
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/renovate-bot.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v2.9.1 # renovate:managed
    file: renovate-bot.yml

  # Includes a base template for running kaniko easily
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/kaniko.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v1.80.3 # renovate:managed
    file: "kaniko.yml"

  - project: "gitlab-com/gl-infra/platform/runway/runwayctl"
    file: "ci-tasks/service-project/runway.yml"
    inputs:
      runway_service_id: ai-gateway
      image: "$CI_REGISTRY_IMAGE/model-gateway:${CI_COMMIT_SHORT_SHA}"
      runway_version: v2.34.2
  - project: "gitlab-org/quality/pipeline-common"
    file:
      - "/ci/danger-review.yml"
    rules:
      - if: $CI_SERVER_HOST == "gitlab.com"

cache:
  key:
    files:
      - poetry.lock
      - .gitlab-ci.yml
  paths:
    - $PIP_CACHE_DIR
    - $POETRY_CACHE_DIR
    - requirements.txt
    - scripts/lib/
    - scripts/vendor/

.poetry:
  before_script:
    - pip install poetry==1.8.2
    - poetry config virtualenvs.in-project true
    - poetry config cache-dir ${POETRY_CACHE_DIR}
    - poetry export -f requirements.txt --output requirements.txt --without-hashes
    - poetry config --list

.docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

install:
  extends: .poetry
  stage: build
  script:
    - poetry install

build-docker-model-gateway:
  stage: build
  extends:
    - .kaniko_base
  variables:
    KANIKO_BUILD_FILE: Dockerfile
    KANIKO_DESTINATION: "${TARGET_IMAGE}"

tests:
  extends: .poetry
  stage: test
  needs:
    - install
  script:
    - make test-coverage-ci
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    when: always
    expire_in: 1 weeks
    reports:
      junit:
        - .test-reports/*.xml
      coverage_report:
        coverage_format: cobertura
        path: .test-reports/coverage.xml
  variables:
    # Enabling debug mode of asyncio so that the test fails in the following cases:
    # - asyncio checks for coroutines that were not awaited and logs them; this mitigates the “forgotten await” pitfall.
    # - Many non-threadsafe asyncio APIs (such as loop.call_soon() and loop.call_at() methods) raise an exception if they are called from a wrong thread.
    PYTHONASYNCIODEBUG: "True"

container_scanning:
  stage: test
  needs:
    - build-docker-model-gateway
  rules:
    - if: $CONTAINER_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
  variables:
    CS_IMAGE: $TARGET_IMAGE

gemnasium-python-dependency_scanning:
  stage: test
  needs:
    - install
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG

secret_detection:
  stage: test
  needs:
    - install
  cache: {}
  rules:
    - if: $SECRET_DETECTION_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG

semgrep-sast:
  stage: test
  needs:
    - install
  rules:
    - if: $SAST_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
